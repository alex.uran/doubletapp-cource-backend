migrate:
	docker compose run --rm web python src/manage.py migrate $(if $m, api $m,)
	docker compose stop

makemigrations:
	python src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	docker compose run --rm web python src/manage.py createsuperuser
	docker compose stop

collectstatic:
	python src/manage.py collectstatic --no-input

db:
	docker compose up db

command:
	python src/manage.py ${c}

shell:
	python src/manage.py shell

debug:
	python src/manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	isort .
	black --config pyproject.toml .
	flake8 --config setup.cfg

check_lint:
	isort --check --diff .
	black --check --config pyproject.toml .
	flake8 --config setup.cfg

build:
	docker compose build

up:
	docker compose up -d

down:
	docker compose down