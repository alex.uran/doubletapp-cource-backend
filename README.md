## doubletapp-cource-backend

# Run bot

- Create .env with variables like in .env.example

- Run command `make bot`

- Bot help command `/help`

# API

- User information at `/api/me/<id>`
