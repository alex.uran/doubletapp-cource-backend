from django.core.management import BaseCommand

from app.internal.bot import Bot
from config.settings import TG_BOT_TOKEN


class Command(BaseCommand):
    help = "Start Telegram bot"

    def handle(self, *args, **kwargs):
        bot = Bot(TG_BOT_TOKEN)
        print("Starting bot")
        bot.run()
