from django.contrib import admin

from app.internal.admin.admin_user import AdminUserAdmin
from app.internal.admin.banc_card import BankCard
from app.internal.admin.bank_account import BankAccountAdmin
from app.internal.admin.telegram_user import TelegramUserAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
