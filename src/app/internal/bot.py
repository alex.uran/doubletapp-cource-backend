from telegram.ext import Application, CommandHandler

from app.internal.transport.bot.commands import (
    get_accounts_command,
    get_cards_command,
    help_command,
    me_command,
    set_phone_command,
    start_command,
)
from app.internal.transport.bot.handlers import (
    get_my_bank_accounts_info_handler,
    get_my_bank_cards_info_handler,
    get_my_info_handler,
    help_handler,
    set_phone_handler,
    start_handler,
)


class Bot:
    def __init__(self, token: str):
        self.application = Application.builder().token(token).build()

        self.application.add_handler(CommandHandler(help_command, help_handler))
        self.application.add_handler(CommandHandler(start_command, start_handler))
        self.application.add_handler(CommandHandler(set_phone_command, set_phone_handler))
        self.application.add_handler(CommandHandler(me_command, get_my_info_handler))
        self.application.add_handler(CommandHandler(get_accounts_command, get_my_bank_accounts_info_handler))
        self.application.add_handler(CommandHandler(get_cards_command, get_my_bank_cards_info_handler))

    def run(self):
        self.application.run_polling()
