from django.http import HttpRequest, HttpResponseForbidden, HttpResponseNotFound, JsonResponse
from django.views import View

from app.internal.services.user_service import get_telegram_user_by_id


class MyInfoView(View):
    @staticmethod
    async def get(request: HttpRequest, telegram_id: int) -> JsonResponse:
        telegram_user = await get_telegram_user_by_id(telegram_id)
        phone = telegram_user.phone_number

        if phone is None or phone == "":
            return HttpResponseForbidden("Missing phone number")

        if telegram_user is None:
            return HttpResponseNotFound("User not found")

        return JsonResponse(
            {
                "telegram_id": telegram_user.telegram_id,
                "username": telegram_user.username,
                "phone_number": str(telegram_user.phone_number),
            }
        )
