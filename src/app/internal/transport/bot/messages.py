import decimal

from app.internal.transport.bot.commands import (
    get_accounts_command,
    get_cards_command,
    help_command,
    me_command,
    set_phone_command,
    start_command,
)
from config.settings import REGION

start_description = f"/{start_command} - начать работу с ботом."
help_description = f"/{help_command} - показать cooбщение с инструкцией по командам бота."
set_phone_description = f"/{set_phone_command} *phone* - указать номер телефона."
me_description = f"/{me_command} - получить информацию о себе."
get_accounts_description = f"/{get_accounts_command} - получить информацию о счетах."
get_cards_description = f"/{get_cards_command} - получить информацию о картах."

help_ = (
    start_description
    + "\n"
    + set_phone_description
    + "\n"
    + me_description
    + "\n"
    + help_description
    + "\n"
    + get_accounts_description
    + "\n"
    + get_cards_description
)

user_registered = "Данные собраны: id, username."
phone_saved = "Номер телефона записан."


need_auth = "Сначала необходимо ввести /start для сбора данных."
missing_phone = f"Команда недоступна, отсутствует номер телефона. Введите /{set_phone_command} *phone*"
invalid_arguments = f"Неправильные аргументы для команды, посмотрите /{help_command} "
invalid_phone = f"Вы ввели невалидный номер телефона, попробуйте ещё раз, регион для вашего номера {REGION}."
try_again = "Упс, что-то пошло не так, попробуйте ещё раз."

no_bank_accounts = "У вас пока что нет ни одного счета."
no_bank_cards = "У вас пока что нет ни одной карты."


def format_me_command_message(telegram_id: int, username: str, phone: str) -> str:
    return f"Telegram id: `{telegram_id}`\n" + f"Username: `{username}`\n" + f"Phone: `{phone}`"


def format_my_accounts_message(account_id_to_amount: dict[int, decimal]) -> str:
    return "\n".join(
        [f"Id счета: {acc_id}; Сумма на счёте: {acc_amount};" for acc_id, acc_amount in account_id_to_amount.items()]
    )


def format_my_cards_message(card_id_to_amount: dict[int, decimal]) -> str:
    return "\n".join(
        [
            f"Id карты: {card_id}; Сумма на счёте карты: {card_amount};"
            for card_id, card_amount in card_id_to_amount.items()
        ]
    )
