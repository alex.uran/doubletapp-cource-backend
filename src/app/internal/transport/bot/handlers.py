from telegram import Update
from telegram.ext import CallbackContext

from app.internal.models.errors import (
    InvalidArgumentsException,
    MissingPhoneException,
    PhoneNumberInvalidException,
    UserNotFoundException,
)
from app.internal.models.telegram_user import TelegramUser
from app.internal.services.bank_account_service import get_bank_accounts_by_user_id
from app.internal.services.bank_card_service import get_bank_cards_by_user_id
from app.internal.services.user_service import (
    create_telegram_user,
    get_telegram_user_by_id,
    update_telegram_user_phone_number_by_id,
)
from app.internal.transport.bot import messages
from app.internal.transport.bot.decorators import prevent_error


@prevent_error(Exception, messages.try_again)
async def help_handler(update: Update, context: CallbackContext):
    await update.message.reply_text(messages.help_)


@prevent_error(Exception, messages.try_again)
async def start_handler(update: Update, context: CallbackContext):
    user = update.effective_user
    telegram_id = user.id
    username = user.username

    await create_telegram_user(telegram_id, username)
    await update.message.reply_text(messages.user_registered)


@prevent_error(Exception, messages.try_again)
@prevent_error(UserNotFoundException, messages.need_auth)
@prevent_error(PhoneNumberInvalidException, messages.invalid_phone)
@prevent_error(InvalidArgumentsException, messages.invalid_arguments)
async def set_phone_handler(update: Update, context: CallbackContext):
    try:
        phone_number = context.args[0]
    except IndexError:
        raise InvalidArgumentsException

    user = update.effective_user
    telegram_id = user.id
    await update_telegram_user_phone_number_by_id(telegram_id, phone_number)
    await update.message.reply_text(messages.phone_saved)


@prevent_error(Exception, messages.try_again)
@prevent_error(MissingPhoneException, messages.missing_phone)
@prevent_error(UserNotFoundException, messages.need_auth)
async def get_my_info_handler(update: Update, context: CallbackContext):
    telegram_user = await get_telegram_user_by_id(update.effective_user.id)
    __ensure_user_has_phone_number(telegram_user)

    await update.message.reply_text(
        messages.format_me_command_message(
            telegram_user.telegram_id,
            telegram_user.username if telegram_user.username is not None else "*Отсутствует*",
            telegram_user.phone_number,
        )
    )


@prevent_error(Exception, messages.try_again)
@prevent_error(MissingPhoneException, messages.missing_phone)
@prevent_error(UserNotFoundException, messages.need_auth)
async def get_my_bank_accounts_info_handler(update: Update, context: CallbackContext):
    telegram_user = await get_telegram_user_by_id(update.effective_user.id)
    __ensure_user_has_phone_number(telegram_user)

    accounts = await get_bank_accounts_by_user_id(update.effective_user.id)
    if len(accounts) == 0:
        await update.message.reply_text(messages.no_bank_accounts)

    await update.message.reply_text(messages.format_my_accounts_message(dict((acc.id, acc.amount) for acc in accounts)))


@prevent_error(Exception, messages.try_again)
@prevent_error(MissingPhoneException, messages.missing_phone)
@prevent_error(UserNotFoundException, messages.need_auth)
async def get_my_bank_cards_info_handler(update: Update, context: CallbackContext):
    telegram_user = await get_telegram_user_by_id(update.effective_user.id)
    __ensure_user_has_phone_number(telegram_user)

    cards = await get_bank_cards_by_user_id(update.effective_user.id)
    if len(cards) == 0:
        await update.message.reply_text(messages.no_bank_cards)

    await update.message.reply_text(
        messages.format_my_cards_message(dict((card.id, card.bank_account.amount) for card in cards))
    )


def __ensure_user_has_phone_number(user: TelegramUser):
    phone = user.phone_number
    if phone is None or phone == "":
        raise MissingPhoneException
