from typing import Callable

from telegram import Update
from telegram.ext import CallbackContext


def prevent_error(error_type: type, reply_message: str):
    def wrapper(func: Callable):
        async def wrapped_func(update: Update, context: CallbackContext):
            try:
                return await func(update, context)
            except error_type:
                await update.message.reply_text(reply_message)
                return

        return wrapped_func

    return wrapper
