from django.contrib import admin

from app.internal.models.bank_account import BankAccount


@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    list_display = ("id", "telegram_user", "amount")
