from django.contrib import admin

from app.internal.models.banc_card import BankCard


@admin.register(BankCard)
class BankCardAdmin(admin.ModelAdmin):
    list_display = ("id", "bank_account")
