from django.urls import path

from app.internal.transport.rest.handlers import MyInfoView

urlpatterns = [path("me/<int:telegram_id>", MyInfoView.as_view())]
