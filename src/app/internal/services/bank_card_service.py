from asgiref.sync import sync_to_async

from app.internal.models.banc_card import BankCard


@sync_to_async
def get_bank_cards_by_user_id(telegram_id: int) -> list[BankCard]:
    return list(BankCard.objects.filter(bank_account__telegram_user_id=telegram_id).select_related("bank_account"))
