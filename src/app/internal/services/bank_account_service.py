from asgiref.sync import sync_to_async

from app.internal.models.bank_account import BankAccount


@sync_to_async
def get_bank_accounts_by_user_id(telegram_id: int) -> list[BankAccount]:
    return list(BankAccount.objects.filter(telegram_user_id=telegram_id))
