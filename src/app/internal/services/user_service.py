from phonenumber_field.phonenumber import to_python

from app.internal.models.errors import PhoneNumberInvalidException, UserNotFoundException
from app.internal.models.telegram_user import TelegramUser
from config.settings import REGION


async def create_telegram_user(telegram_id: int, username: str):
    user, is_created = await TelegramUser.objects.aupdate_or_create(
        telegram_id=telegram_id, defaults={"username": username}
    )
    return user


async def get_telegram_user_by_id(telegram_id: int) -> TelegramUser:
    user = await TelegramUser.objects.filter(telegram_id=telegram_id).afirst()
    if user is None:
        raise UserNotFoundException

    return user


async def update_telegram_user_phone_number_by_id(telegram_id: int, phone_number: str):
    __ensure_phone_number_is_valid(phone_number)
    rows_count = await TelegramUser.objects.filter(telegram_id=telegram_id).aupdate(phone_number=phone_number)
    if rows_count == 0:
        raise UserNotFoundException


def __ensure_phone_number_is_valid(phone_number: str):
    if not to_python(phone_number, region=REGION).is_valid():
        raise PhoneNumberInvalidException
