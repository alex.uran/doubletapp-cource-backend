from django.db import models

from app.internal.models.bank_account import BankAccount


class BankCard(models.Model):
    id = models.BigIntegerField(primary_key=True)
    bank_account = models.ForeignKey(BankAccount, on_delete=models.CASCADE)

    class Meta:
        db_table = "BankCards"
