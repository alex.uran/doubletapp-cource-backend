from django.db import models

from app.internal.models.telegram_user import TelegramUser


class BankAccount(models.Model):
    id = models.BigIntegerField(primary_key=True)
    telegram_user = models.ForeignKey(TelegramUser, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=20, decimal_places=2)

    class Meta:
        db_table = "BankAccounts"
