from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from config.settings import REGION


class TelegramUser(models.Model):
    telegram_id = models.BigIntegerField(primary_key=True)
    username = models.CharField(unique=True, max_length=32, blank=True, null=True)
    phone_number = PhoneNumberField(region=REGION, max_length=16, blank=True)

    class Meta:
        ordering = ["username"]
        verbose_name_plural = "Telegram users"
