class UserNotFoundException(Exception):
    message = "User not found"


class MissingPhoneException(Exception):
    message = "User doesn't have phone"


class PhoneNumberInvalidException(Exception):
    message = "Phone number is invalid"


class InvalidArgumentsException(Exception):
    message = "Arguments is invalid"
